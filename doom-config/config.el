;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-
;; $DOOMDIR/config.el  -*- lexical-binding: t; -*-


;; #+begin_quote
;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

;;  Doom exposes five (optional) variables for controlling fonts in Doom. Here
;;  are the three important ones:

;;  + `doom-font'
;;  + `doom-variable-pitch-font'
;;  + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;    presentations or streaming.

;;  They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;;  font string. You generally only need these two:

;; Here are some additional functions/macros that could help you configure Doom:

;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys

;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.

;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
;; #+end_quote


;; - [[(doom-whoami)][Some functionality uses this to identify you, e.g. GPG configuration, email
;;  clients, file templates and snippets.]]
;; - as a [[(doom-font)][font i want sans with big size]]
;; - There are two ways to load a theme. Both assume the theme is installed and available. You can either set `doom-theme' or manually load a theme with the `load-theme' function. i will use this theme [[(doom-theme)]]
;; - [[(doom-set-org-dir)][set org directory]]  If you use `org' and don't want your org files in the default location below change `org-directory'. It must be set before org loads!
;; - [[(doom-config-line-numbers)][tell doom how to handle line numbers]]  This determines the style of line numbers in effect. If set to `nil', line numbers are disabled. For relative line numbers, set this to `relative'.
;; - [[(doom-use-gpg2)][let doom verify we are using gpg2]]  ensure we are using gpg2



;; #+NAME: doom-config-config.el

;; [[git:/mnt/conformity_ONBOARD/conformity_CODJEX/green_codjex/ataleo_campaign/ataleo_worclones/dotfiles/dotfiles.org::master@{2022-05-10}::154][doom-config-config.el]]
(setq user-full-name "collibro"
      user-mail-address "9834959-collibroa@users.noreply.gitlab.com") ;  (ref:doom-whoami)
;;(setq doom-font "SF Mono-13") ;; a real font that even mac should find
;;(setq doom-font (font-spec :family "monospace" :size 28 :weight 'semi-light)      doom-variable-pitch-font (font-spec :family "sans" :size 29)) ; (ref:doom-font)
(setq doom-theme 'doom-henna)                            ;      (ref:doom-theme)
(setq org-directory "~/org/")                           ; (ref:doom-set-org-dir)
(setq display-line-numbers-type t) ;             (ref:doom-config-line-numbers)

(setq doom-font (font-spec :family "Monaco" :size 14)
      ;; https://github.com/edwardtufte/et-book/blob/gh-pages/et-book/et-book-roman-line-figures/et-book-roman-line-figures.ttf
      doom-variable-pitch-font (font-spec :family "didot" :size 16)
      display-line-numbers-type nil
      confirm-kill-emacs nil
      +format-on-save-enabled-modes '(python-mode)
      )
(setq epg-gpg-program "gpg2") ;                             (ref:doom-use-gpg2)

(defalias 'oreday #'my/org-refile-to-datetree)
(defalias 'orefile #'org-refile)
(map!
 :after '(org)
 :map org-mode-map
 :leader
 :desc "refile to day" "rd"  #'oreday )
;; doom-config-config.el ends here


;; a helper to load my custom elisp stuff
;; elisp



(defun load-directory (dir)
  "Load all files in target directory DIR recursively.

		use this for loading custom Lisp code"
  (let ((load-it (lambda (f) (load-file (concat (file-name-as-directory dir) f)))))
    (mapc load-it (directory-files dir nil "\\.el$"))))


;; configure some refile helpers
;; elisp
;; - refile to unorganizer
;; - refile to journal
;; - refile to meetings



(use-package! org-reverse-datetree
 ;; :bind (:map org-mode-map ("C-c rj" . my/org-refile-to-journal) ("C-c rv" . orecap))
  :init

  (defun my/org-refile-to-unorganizer (arg)
    (interactive "P")
    (org-reverse-datetree-refile-to-file "~/org/unorganizer.org" nil
                                         :ask-always arg :prefer '("CREATED_TIME" "CREATED" "CREATED_AT" "CLOSED")))


  (defun my/org-refile-to-journal (arg)
    (interactive "P")
    (org-reverse-datetree-refile-to-file "~/org/journal.org" nil
                                         :ask-always arg :prefer '("CREATED_TIME" "CREATED" "CREATED_AT" "CLOSED"))) ; (org-refile-to-journal-reverse)

  (defun my/org-refile-to-meetings (arg)
    (interactive "P")
    (org-reverse-datetree-refile-to-file "~/org/stores/meetings.org" nil
                                         :ask-always arg :prefer '("CREATED_TIME" "CREATED" "CREATED_AT" "CLOSED")))
  (defalias 'oresurface #'my/org-refile-to-unorganizer)
  (defalias 'orecap #'my/org-refile-to-unorganizer)

  :config
  (add-to-list 'org-capture-templates

           '("M" "Meeting entry" entry
                 (file+function "~/org/stores/meetings.org" org-reverse-datetree-goto-date-in-file)
                 (file "~/placemarks/myTemplates/org-capture-templates/_capture-entry.org" ) :unnarrowed t :empty-lines 1 :clock-in t :clock-resume t :empty-lines 1))
  )
(map!
 :after '(org-reverse-datetree)
 :map org-mode-map
 :leader
 :desc "refile to unorganizer" "ru"  #'orecap )


;; want to use org for diary entries in doom aswell


(setq org-agenda-diary-file "~/org/collections.org") ; (org-diary-file)


;; configuring dired+

(use-package! dired+)
(map!
 :after '(dired)
 :map dired-mode-map
 :leader
 :desc "toggle details" "td"  #'dired-hide-details-mode
 :prefix ("id" . "add to dired")
 :desc "add to buffer" "a" #'diredp-add-to-dired-buffer
 :desc "Add in other window" "4" #'diredp-add-to-dired-buffer-other-window)


;; enable ob async

(require 'ob-async)

;; doom bookmarks plus extension

;; configure bookmark plus[fn:5] in doom
;; #+NAME: doom-config-bookmark+

(use-package bookmark+)
(map!
   :map bookmark-bmenu-mode-map
   :leader
   :desc "toggle details" "td"  #'bookmark-bmenu-toggle-filenames)

;; doom elisp literacy extension
;; config for elisp format
;; #+NAME: doom-config-elisp-format

(use-package! elisp-format
  :config
  (setq elisp-format-column 90)
  (setq elisp-format-newline-keyword-addons-list
        '("interactive" "setq" "set" "buffer-substring" "buffer-substring-no-properties"))
  (setq elisp-format-split-subexp-keyword-addons-list
        '("and" "or" "buffer-substring" "buffer-substring-no-properties" "font-lock-add-keywords" "add-to-list"))

  :commands (elisp-format-buffer))

;; doom dart literacy extension

;; configure projectile mode for dart development

(use-package! projectile
  :config (setq my/flutter-related-files (list
                                          (projectile-related-files-fn-test-with-suffix
                                           "dart" "_test")))
  (setq projectile-create-missing-test-files t)
  (setq projectile-track-known-projects-automatically nil)
  (projectile-register-project-type 'dart '("pubspec.yaml" "lib" ".packages" ".dart_tool")
                                    :project-file ".packages"
                                    :compile "flutter build appbundle"
                                    :src-dir "lib/"
                                    :test "flutter test"
                                    :test-dir "test/"
                                    :test-suffix "_test"
                                    ;;   :test-prefix ""
                                    :related-files-fn `(list
                                                        ,(projectile-related-files-fn-test-with-suffix
                                                          "dart" "_test"))))


;; configure lsp for flutter

(use-package! lsp-dart
  :config
  (add-to-list 'compilation-error-regexp-alist 'dart-analyze)
  (add-to-list 'compilation-error-regexp-alist-alist '(dart-analyze
                                                       "\\([^ ]*\\.dart\\):\\([0-9]+\\):\\([0-9]+\\)"
                                                       1 2 3)))


;; configure dart mode

(use-package! dart-mode
  :init (defun project-try-dart (dir)
          ""
          "Assists project.el in finding the project root for your dart file."
          ""
          (let ((project (or (locate-dominating-file dir "pubspec.yaml")
                             (locate-dominating-file dir "BUILD"))))
            (if project (cons 'dart project)
              (cons 'transient dir))))
  (add-hook 'project-find-functions #'project-try-dart)
  (cl-defmethod
    project-root
    ((project (head dart)))
    (list (cdr project)))
  ;; Help lsp-mode find your project root automatically.
  (with-eval-after-load "projectile" (add-to-list 'projectile-project-root-files-bottom-up
                                                  "pubspec.yaml")
                        (add-to-list 'projectile-project-root-files-bottom-up "BUILD"))
  :config)


;; teach rainbow mode about dart colors

(use-package! rainbow-mode
  :config
  (add-to-list 'rainbow-hexadecimal-colors-font-lock-keywords '("\\(?:0[xX]\\{1\\}\\)\\(?:[0-9a-fA-F]\\{2\\}\\)\\([0-9a-fA-F]\\{6\\}\\)"
  (1   (rainbow-colorize-match (rainbow-colorize-hexadecimal-without-sharp)   )))t ))


;; configure slack.el in doom

(use-package! slack
  :commands (slack-start)
  :init (setq slack-buffer-emojify t) ;; if you want to enable emoji, default nil
  (setq slack-prefer-current-team t)
  (load (expand-file-name "sensitive.el" "~/SENSITIVE"))
  :config (require 'sensitive)
  (evil-define-key 'normal slack-info-mode-map ",u" 'slack-room-update-messages)
  (evil-define-key 'normal slack-mode-map ",c" 'slack-buffer-kill ",ra"
    'slack-message-add-reaction ",rr" 'slack-message-remove-reaction ",rs"
    'slack-message-show-reaction-users ",pl" 'slack-room-pins-list ",pa"
    'slack-message-pins-add ",pr" 'slack-message-pins-remove ",mm"
    'slack-message-write-another-buffer ",me" 'slack-message-edit ",md"
    'slack-message-delete ",u" 'slack-room-update-messages ",2"
    'slack-message-embed-mention ",3" 'slack-message-embed-channel "\C-n"
    'slack-buffer-goto-next-message "\C-p" 'slack-buffer-goto-prev-message)
  (evil-define-key 'normal slack-edit-message-mode-map ",k" 'slack-message-cancel-edit
    ",s" 'slack-message-send-from-buffer ",2" 'slack-message-embed-mention ",3"
    'slack-message-embed-channel))
(use-package! alert
  :commands (alert)
  :init (setq alert-default-style 'notifier))
