(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auth-sources
   '("/home/detleva/doom-emacs/.local/etc/authinfo.gpg" "~/.authinfo.gpg" password-store))
 '(bmkp-last-as-first-bookmark-file "~/doom-emacs/.local/etc/bookmarks")
 '(org-agenda-files
   '("~/org/stores/meetings.org" "/home/detleva/org/collections.org" "/home/detleva/org/journal.org" "/home/detleva/org/notes.org" "/home/detleva/org/org.org" "/home/detleva/org/projects.org" "/home/detleva/org/todo.org"))
 '(rainbow-html-colors-major-mode-list '(html-mode css-mode php-mode nxml-mode xml-mode dart-mode))
 '(rainbow-x-colors-major-mode-list
   '(emacs-lisp-mode lisp-interaction-mode c-mode c++-mode java-mode dart-mode))
 '(safe-local-variable-values '((projectile-project-type . "flutter-confidence"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
