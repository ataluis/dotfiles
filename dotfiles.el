;; publishing
;; i keep a seperate emacs profile for publishing
;; i can publish my dotfiles with

(load-file "publish-dotfiles.el")
(org-publish-all)
