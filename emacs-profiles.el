;; emacs profiles
;; Note that this is different from Chemacs 1. Before Chemacs installed itself as ~/.emacs and you could have your own default setup in ~/.config/emacs.d. This approach no longer works because of ~/.config/emacs.d/early-init.el, so Chemacs 2 needs to be installed as ~/.config/emacs.d.
;; First, install Doom somewhere: git clone https://github.com/hlissner/doom-emacs ~/doom-emacs ~/doom-emacs/bin/doom install
;; Next you will need to create a ~/.emacs-profiles.el file, for details see below.

(("default" . ((user-emacs-directory . "~/.config/emacs.default")))


 ("doom"   . ((user-emacs-directory . "~/doom-emacs/")
	      (env . (("DOOMDIR" . "~/.doom.d/")))
              )))
