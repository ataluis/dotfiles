
;; lets publish dotfiles here is the minimum publish project alist
;; try seperating setup from settings and config
;; the publish-dotfiles-setup.el file

;; Commentary:
;; This script will convert the org-mode files in this directory into
;; html.
;; #+NAME: publish-dotfiles-setup

;; Code:
(defvar bootstrap-version)
(setq user-emacs-directory "~/.emacs-publish/")
(let ((bootstrap-file (expand-file-name "straight/repos/straight.el/bootstrap.el"
                                        user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer (url-retrieve-synchronously
                          "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
                          'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))
(straight-use-package 'htmlize)
(straight-use-package 'org)
(straight-use-package 'bookmark+)
(straight-use-package 'elfeed)
(straight-use-package 'dash)
(straight-use-package 'exec-path-from-shell)
(require 'bookmark+-autoloads)
(require 'ox-html)
(require 'dash)
(require 'easy-mmode)
(require 'ox)
(require 'org)
(require 'ox-publish)
(require 'ol)
(require 'org-attach)
