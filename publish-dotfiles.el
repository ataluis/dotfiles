
;; actual publishing script

;; #+NAME: tangle-publish-dotfiles-el

(load-file "~/.dotfiles/publish-dotfiles-setup.el")
(defvar site-attachments
  (regexp-opt '("jpg" "jpeg" "gif" "png" "svg" "ico" "cur" "css" "js" "woff" "html"
                "pdf"))
  "File types that are published as static files.")
(setq org-publish-project-alist `(( "dotfiles-site-org" :base-directory "~/.dotfiles"
                                    :base-extension "org"
                                    :recursive t
                                    :publishing-function org-html-publish-to-html
                                    :publishing-directory
                                    "~/placemarks/org_published/dotfiles/html"
                                    :html-head
                                    "<link rel=\"stylesheet\" href=\"https://unpkg.com/keyboard-css@1.2.4/dist/css/main.min.css\" /><link rel=\"stylesheet\" href=\"../../org.css\" type=\"text/css\"/><link rel=\"shortcut icon\" href=\"assets/favicon.ico\"/>"

                                    :exclude "_.*"
                                    :htmlized-source t
                                    :makeindex nil
                                    :auto-sitemap nil
                                    :sitemap-filename "_sitemap.org"
                                    :sitemap-file-entry-format "%d *%t*"
                                    :with-sub-superscript nil
                                    :html-html5-fancy t
                                    :with-tables t)

                                  ;; note - dont plan on keeping too many non-org files in the main org repository
                                  ( "dotfiles-site-static" :base-directory "~/.dotfiles"
                                    :exclude "public/"
                                    :base-extension ,site-attachments
                                    :publishing-directory
                                    "~/placemarks/org_published/dotfiles/html"
                                    :publishing-function org-publish-attachment
                                    :recursive t)
                                  ( "dotfiles" :components ("dotfiles-site-static"
                                                            "dotfiles-site-org"))))
